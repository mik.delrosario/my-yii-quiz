<?php
namespace app\components\widget;

use yii\base\Widget;
use yii\helpers\Html;

class NewWidget extends Widget
{
    public $images;
    public $fx;
    public $sync;
    public $delay;
    public $easing;
    public $next;
    public $timeout;

    public $div;
    public $script;

    public function init()
    {
        parent::init();

        $generateID = uniqid();
        $temp = true;

        $this->div = '<div id="'.$generateID.'" class="pics" style="position: relative;">';
        foreach ($this->images as $key => $value) {
        	if($temp){
        		$temp = false;
        		$this->div = $this->div.'<img src="'.$value.'" width="200" height="200" style="position: absolute; top: 0px; left: 0px; display: block; z-index: 3; opacity: 1; width: 200px; height: 200px;">';
        	}else{
        		$this->div = $this->div.'<img src="'.$value.'" width="200" height="200" style="position: absolute; top: 100px; left: 100px; display: none; z-index: 4; width: 0px; height: 0px; opacity: 1;">';
        	}
        }
        $this->div = $this->div.'</div>';
        $script = '';
        if(!empty($this->fx)) {
        	if($script != '') $script = $script.',';
        	$script = $script.'fx:"'.$this->fx.'"';
        }
        if(!empty($this->sync)) {
        	if($script != '') $script = $script.',';
        	$script = $script.'sync:"'.$this->sync.'"';
        }
        if(!empty($this->delay)) {
        	if($script != '') $script = $script.',';
        	$script = $script.'delay:"'.$this->delay.'"';
        }
        if(!empty($this->easing)) {
        	if($script != '') $script = $script.',';
        	$script = $script.'easing:"'.$this->easing.'"';
        }
        if(!empty($this->next)) {
        	if($script != '') $script = $script.',';
        	$script = $script.'next:"'.$this->next.'"';
        }
        if(!empty($this->timeout)) {
        	if($script != '') $script = $script.',';
        	$script = $script.'timeout:"'.$this->timeout.'"';
        }
        $script = '<script> $("#'.$generateID.'").cycle({ '.$script.' });</script>';

        $this->script = $script;
    }

    public function run()
    {
        echo $this->div.' '.$this->script;
    }
}