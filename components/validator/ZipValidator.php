<?php
namespace app\components\validator;

use yii\validators\Validator;

class ZipValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
    	if(strtolower(trim($model->u_country)) == 'usa'){
    		if(strlen($model->$attribute) != 5) $this->addError($model, $attribute, 'USA zip must be 5 characters');
    	}
        
    }
}