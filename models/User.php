<?php

namespace app\models;

use Yii;
use app\components\validator\ZipValidator;

class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['u_name',function ($attribute, $params) {
                $temp = explode(' ', $this->$attribute);
                $passed = true;
                foreach ($temp as $key => $value) {
                    if(strlen($value) <= 2) $passed = false;
                }
                if(count($temp) < 2 || trim($this->$attribute) == ''){
                    $this->addError($attribute, 'Name must contain atleast 2 words.');
                }
                
                
                if(!$passed){
                    $this->addError($attribute, 'Name must contain morethan 2 characters.');
                }
            }],
            ['u_city', function ($attribute, $params) {
                if(strlen($this->$attribute) <= 2){
                    $this->addError($attribute, 'City length should be > 2.');
                } 
            }],
            ['u_zip', ZipValidator::className()],
            ['u_state','required', 'when' => function ($model) {
                return strtolower($model->u_country) == 'usa';
            }, 'whenClient' => "function (attribute, value) {
                return $.trim($('#u_country').val()) == 'usa';
            }"],
            [['u_country'], 'required'],
            ['u_email', function ($attribute, $params) {
                $temp = explode(',', $this->$attribute);
                $passed = true;
                $invalid_email = '';
                foreach ($temp as $key => $value) {
                    if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                      $passed = false;
                      if(trim($invalid_email) == ""){
                        $invalid_email = $value;
                      }else{
                        $invalid_email = $invalid_email.",".$value;
                      }
                    }
                }

                if(!$passed){
                    $this->addError($attribute, $invalid_email.' not valid Email.');
                    $_POST['User']['email_error'] = $invalid_email.' not valid Email.';
                }else{
                    $_POST['User']['email_error'] = '';
                }
            }]

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'u_name' => 'Name',
            'u_city' => 'City',
            'u_zip' => 'Zip',
            'u_state' => 'State',
            'u_country' => 'Country',
            'u_email' => 'Email',
        ];
    }
}
