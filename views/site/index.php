<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\components\widget\NewWidget;

?>
<div class="User">

    <?php echo NewWidget::widget(
            array(
                'images'=>array(
                    "http://malsup.github.com/images/beach1.jpg",
                    "http://malsup.github.com/images/beach2.jpg",
                    "http://malsup.github.com/images/beach3.jpg"
                ),
                'fx'=>'zoom',
                'sync'=>false,
                'delay'=>-2000)
            ); ?> 
              

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'u_name') ?>
        <?= $form->field($model, 'u_city') ?>
        <?= $form->field($model, 'u_zip') ?>
        <?= $form->field($model, 'u_state') ?>
        <?= $form->field($model, 'u_country') ?>

        <div>
            <div class="form-group field-user-u_email has-error">
                <label for="u_email">Emails:</label>
                <input type="text" id="user-u_email" name="User[u_email]" value="<?php echo (isset($_POST['User']['u_email'])) ? $_POST['User']['u_email'] : ''; ?>" />
                <div class="help-block"><?php echo (isset($_POST['User']['email_error'])) ? $_POST['User']['email_error'] : ''; ?></div>
            </div>
            <script>
            $('#user-u_email').selectize({
                persist: false,
                createOnBlur: true,
                create: true
            });
            </script>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>

