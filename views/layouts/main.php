<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="<?php echo Yii::getAlias('@web').'/css/normalize.css'; ?>">
    <link rel="stylesheet" href="<?php echo Yii::getAlias('@web').'/css/stylesheet.css'; ?>">
    <!--[if IE 8]><script src="js/es5.js"></script><![endif]-->
    <script src="<?php echo Yii::getAlias('@web').'/js/jquery.min.js'; ?>"></script>
    <script src="<?php echo Yii::getAlias('@web').'/dist/js/standalone/selectize.js'; ?>"></script>
    <script src="<?php echo Yii::getAlias('@web').'/js/index.js'; ?>"></script>
    <script src="<?php echo Yii::getAlias('@web').'/cycle-master/jquery.cycle.all.js'; ?>"></script>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        
    </footer>

<?php $this->endBody() ?>

<script type="text/javascript">
    $(document).ready(function(){
        
    });
</script>

</body>
</html>
<?php $this->endPage() ?>
