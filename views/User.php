<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form ActiveForm */
?>
<div class="User">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'user_id') ?>
        <?= $form->field($model, 'u_name') ?>
        <?= $form->field($model, 'u_city') ?>
        <?= $form->field($model, 'u_zip') ?>
        <?= $form->field($model, 'u_state') ?>
        <?= $form->field($model, 'u_country') ?>
        <?= $form->field($model, 'u_email') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- User -->
